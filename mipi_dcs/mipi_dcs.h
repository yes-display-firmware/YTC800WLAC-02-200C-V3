#ifndef __MIPI_DCS_H__
#define __MIPI_DCS_H__

#include <stdint.h>
#include "lt9211.h"

void LCD_Reset(void);
void InitPanel(void);

#endif //__MIPI_DCS_H__
