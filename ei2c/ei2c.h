#ifndef __EI2C_H__
#define __EI2C_H__


#include "bsp.h"
#include "stdint.h"


void ei2c_sda(uint8_t state);
void ei2c_scl(uint8_t state);
void ei2c_sdaOut(void);
void ei2c_sdaIn(void);
uint8_t ei2c_getSda(void);
void ei2c_delayUs(uint32_t uscount);
void ei2c_start(void);
void ei2c_stop(void);
void  ei2c_sendByte(uint8_t c);
uint8_t  ei2c_rcvByte(void);
void ei2c_ack(uint8_t a);

extern void ei2c_init(void);
extern uint8_t ei2c_memWrite(uint8_t sla,uint8_t suba,uint8_t *s,uint8_t no);
extern uint8_t ei2c_memRead(uint8_t sla,uint8_t suba,uint8_t *s,uint8_t no);
uint8_t ei2c_read1Byte(uint8_t RegAddr);
uint8_t ei2c_readNByte(uint8_t RegAddr,uint8_t *p_data,uint8_t N);
uint8_t ei2c_write1Byte(uint8_t RegAddr, uint8_t d);
uint8_t ei2c_writeNByte(uint8_t RegAddr, uint8_t *d,uint8_t N);

#endif //__EI2C_H__