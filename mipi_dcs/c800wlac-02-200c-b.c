#include "c800wlac-02-200c-b.h"
#include "stdio.h"
#include "systick.h"

void GpioInitial(void)
{
	do{
		rcu_periph_clock_enable(HX8394_RST_GPIO_CLK);
		
		gpio_mode_set(HX8394_RST_GPIO_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, HX8394_RST_PIN);
		gpio_output_options_set(HX8394_RST_GPIO_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, HX8394_RST_PIN);
		GPIO_BC(HX8394_RST_GPIO_PORT) = HX8394_RST_PIN;
	} while(0);
	
	do{
		rcu_periph_clock_enable(LT9211_RST_GPIO_CLK);
		
		gpio_mode_set(LT9211_RST_GPIO_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, LT9211_RST_PIN);
		gpio_output_options_set(LT9211_RST_GPIO_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, LT9211_RST_PIN);
		GPIO_BC(LT9211_RST_GPIO_PORT) = LT9211_RST_PIN;
	} while(0);
	
	do{
		rcu_periph_clock_enable(LT9211_INT_GPIO_CLK);
		rcu_periph_clock_enable(RCU_CFGCMP);

		gpio_mode_set(LT9211_INT_GPIO_PORT, GPIO_MODE_INPUT, GPIO_PUPD_PULLUP, LT9211_INT_PIN);
	} while(0);
	
	do {
		/* enable COM GPIO clock */
		rcu_periph_clock_enable(RCU_GPIOA);

		/* connect port to USARTx_Tx */
		gpio_af_set(GPIOA, GPIO_AF_1, GPIO_PIN_2);

		/* connect port to USARTx_Rx */
		gpio_af_set(GPIOA, GPIO_AF_1, GPIO_PIN_3);

		/* configure USART Tx as alternate function push-pull */
		gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_2);
		gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_2);

		/* configure USART Rx as alternate function push-pull */
		gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_3);
		gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_3);
	} while(0);
	
	/*do {
		// enable GPIOA clock 
		rcu_periph_clock_enable(RCU_GPIOA);
		// enable I2C0 clock 
		rcu_periph_clock_enable(RCU_I2C0);

		// connect PB6 to I2C0_SCL 
		gpio_af_set(GPIOA, GPIO_AF_1, GPIO_PIN_9);
		// connect PB7 to I2C0_SDA 
		gpio_af_set(GPIOA, GPIO_AF_1, GPIO_PIN_10);
		// configure GPIO pins of I2C0 
		gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP,GPIO_PIN_9);
		gpio_output_options_set(GPIOA, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ,GPIO_PIN_9);
		gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP,GPIO_PIN_10);
		gpio_output_options_set(GPIOA, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ,GPIO_PIN_10);
	} while(0);*/
}

/*!
    \brief      initilize the USART configuration of the com
    \param[in]  none
    \param[out] none
    \retval     none
*/
void Uart1Initial(void)
{
	/* enable USART clock */
	rcu_periph_clock_enable(RCU_USART1);

	/* USART configure */
	usart_deinit(USART1);
	usart_word_length_set(USART1, USART_WL_8BIT);
	usart_stop_bit_set(USART1, USART_STB_1BIT);
	usart_parity_config(USART1, USART_PM_NONE);
	usart_baudrate_set(USART1, 115200U);
	usart_receive_config(USART1, USART_RECEIVE_ENABLE);
	usart_transmit_config(USART1, USART_TRANSMIT_ENABLE);

	usart_enable(USART1);
}

/* retarget the C library printf function to the USART */
int fputc(int ch, FILE *f)
{
    usart_data_transmit(USART1, (uint8_t) ch);
    while(RESET == usart_flag_get(USART1, USART_FLAG_TBE));
    return ch;
}


#define I2C0_SPEED              400000
#define I2C0_SLAVE_ADDRESS7     0xA0

void I2c0Initial(void)
{
    /* enable I2C clock */
    rcu_periph_clock_enable(RCU_I2C0);
    /* configure I2C clock */
    i2c_clock_config(I2C0,I2C0_SPEED,I2C_DTCY_2);
    /* configure I2C address */
    i2c_mode_addr_config(I2C0,I2C_I2CMODE_ENABLE,I2C_ADDFORMAT_7BITS,I2C0_SLAVE_ADDRESS7);
    /* enable I2C0 */
    i2c_enable(I2C0);
    /* enable acknowledge */
    i2c_ack_config(I2C0,I2C_ACK_ENABLE);
}

uint8_t LT9211_getInt()
{
	if(gpio_input_bit_get(LT9211_INT_GPIO_PORT, LT9211_INT_PIN) == SET){
		return 1;
	} else {
		return 0;
	}
}

void unitTest(void)
{
	while(1){
		GPIO_TG(HX8394_RST_GPIO_PORT) = HX8394_RST_PIN;
		GPIO_TG(LT9211_RST_GPIO_PORT) = LT9211_RST_PIN;
		delay_1ms(10);
	}
	
	while(1) {
		delay_1ms(100);
		if(LT9211_getInt()) {
			printf("LT9211 Int = 1\r\n");
			GPIO_BOP(HX8394_RST_GPIO_PORT) = HX8394_RST_PIN;
			GPIO_BOP(LT9211_RST_GPIO_PORT) = LT9211_RST_PIN;
		} else {
			printf("LT9211 Int = 0\r\n");
			GPIO_BC(HX8394_RST_GPIO_PORT) = HX8394_RST_PIN;
			GPIO_BC(LT9211_RST_GPIO_PORT) = LT9211_RST_PIN;
		}
	}
}