/**
 * @file i2c.c
 * @author Zhai Tao (zhaitao.as@outlook.com)
 * @brief 
 * @version 0.1
 * @date 2022-05-09
 * 
 * @copyright zhaitao.as@outlook.com (c) 2022
 * 
 */

/******************************************************************************
  * @project: LT9211
  * @file: i2c.c
  * @author: zll
  * @company: LONTIUM COPYRIGHT and CONFIDENTIAL
  * @date: 2019.04.10
******************************************************************************/

#include "i2c.h"
#include "util.h"
#include "ei2c.h"
#include "printf.h"

uint8_t     I2CADR = 0x5a; 

static uint8_t i2c_write_byte(uint8_t sla,uint8_t suba,uint8_t *s,uint8_t no)
{
	ei2c_memWrite(sla, suba, s, no);
    
	return 1;
}


static uint8_t i2c_read_byte(uint8_t sla,uint8_t suba,uint8_t *s,uint8_t no)
{
	ei2c_memRead(sla, suba, s, no);
	return 1;
}

uint8_t HDMI_ReadI2C_Byte(uint8_t RegAddr)
{
	uint8_t  p_data=0;
	
	if (i2c_read_byte(I2CADR, RegAddr, &p_data,1) != FALSE)
	{
		return p_data;
	}
	return 0;
}

uint8_t HDMI_ReadI2C_ByteN(uint8_t RegAddr,uint8_t *p_data,uint8_t N)
{
	uint8_t flag;
	
	flag = i2c_read_byte(I2CADR, RegAddr, p_data,N);
	
	return flag;
}

uint8_t HDMI_WriteI2C_Byte(uint8_t RegAddr, uint8_t d)
{
	return i2c_write_byte(I2CADR, RegAddr,&d,1);
}

uint8_t HDMI_WriteI2C_ByteN(uint8_t RegAddr, uint8_t *d,uint8_t N)
{
	uint8_t flag;
	
	flag=i2c_write_byte(I2CADR, RegAddr,d,N) ;
	
	return flag;
}
