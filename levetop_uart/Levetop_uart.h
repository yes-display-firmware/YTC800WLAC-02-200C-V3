/**
 * @file Levetop_uart.h
 * @author Zhai Tao (zhaitao.as@outlook.com)
 * @brief 
 * @version 0.1
 * @date 2022-04-24
 * 
 * @copyright zhaitao.as@outlook.com (c) 2022
 * 
 */


#ifndef __LEVETOP_UART_H__
#define __LEVETOP_UART_H__



int levetop_packet_size(const char *content);
int levetop_pack(unsigned char* dest, unsigned cmd, unsigned index, char *c, unsigned long len);
unsigned int Rx_CRC_CCITT(unsigned char *puchMsg, unsigned int usDataLen);


#endif //__LEVETOP_UART_H__
