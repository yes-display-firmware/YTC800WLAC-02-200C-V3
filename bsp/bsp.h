#ifndef __BSP_H__
#define __BSP_H__

#include "gd32e23x.h"

// PA6 LCM_RST
#define LCM_RST_PIN                         GPIO_PIN_6
#define LCM_RST_GPIO_PORT                   GPIOA
#define LCM_RST_GPIO_CLK                    RCU_GPIOA

// PA7 LT92111_RST
#define LT9211_RST_PIN                         GPIO_PIN_7
#define LT9211_RST_GPIO_PORT                   GPIOA
#define LT9211_RST_GPIO_CLK                    RCU_GPIOA

// PB1 LT9211_INT
#define LT9211_INT_PIN                         GPIO_PIN_1
#define LT9211_INT_GPIO_PORT                   GPIOB
#define LT9211_INT_GPIO_CLK                    RCU_GPIOB

// PA9 I2C1_SCL
#define I2C0_SCL_PIN                         GPIO_PIN_9
#define I2C0_SCL_GPIO_PORT                   GPIOA
#define I2C0_SCL_GPIO_CLK                    RCU_GPIOA

// PA10 I2C1_SDA
#define I2C0_SDA_PIN                         GPIO_PIN_10
#define I2C0_SDA_GPIO_PORT                   GPIOA
#define I2C0_SDA_GPIO_CLK                    RCU_GPIOA

void bsp_initial(void);
void _bsp_rcc_initial(void);
void _bsp_gpio_initial(void);
void _bsp_uart_initial(void);
void _bsp_i2c_initial(void);
void _bsp_wdt_initial(void);
void _bsp_wdt_feed(void);

void bsp_gpio_write_lt9211_rst(int v);
void bsp_gpio_write_driver_rst(int v);




#endif //__BSP_H__
