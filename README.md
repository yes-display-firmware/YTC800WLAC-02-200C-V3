# GD32E230F6_Makefile_Project

#### 介绍
GD32E230F6的Makefile工程

#### 安装
确保系统安装了make和arm-none-eabi-gcc
验证:在命令行下arm-none-eabi-gcc -v, 可以打印出gcc版本, 且版本大于等于V7;make -v可以打印make版本

#### 工具链版本
gcc version 8.3.1 20190703 (release) [gcc-8-branch revision 273027] (GNU Tools for Arm Embedded Processors 8-2019-q3-update)

#### Make版本
GNU Make 4.2

#### 产品信息
- 客户:bio-rad
- Panel:TV080WXQ-N88
- IC:ILI9881
- Bridge:LT9211
