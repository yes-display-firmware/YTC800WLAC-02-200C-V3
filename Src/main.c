/*!
    \file    main.c
    \brief   running LED

    \version 2019-02-19, V1.0.0, firmware for GD32E23x
    \version 2020-12-12, V1.1.0, firmware for GD32E23x
*/

/*
    Copyright (c) 2020, GigaDevice Semiconductor Inc.

    Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors
       may be used to endorse or promote products derived from this software without
       specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
OF SUCH DAMAGE.
*/

#include "Levetop_uart.h"
#include "bsp.h"
#include "ei2c.h"
#include "gd32e23x.h"
#include "lt9211.h"
#include "printf.h"
#include "systick.h"
#include <string.h>
#include "lt9211user.h"

#define LCM_NAME "YTC800WLAC-02-200C-V3"
#define V_MAJOR 00
#define STR(s) #s
#define VERSION(a) "FW version:  " STR(a)

const char product_name[64] = LCM_NAME;
const char fw_version[64] = VERSION(V_MAJOR);

void _uart_send(const char *buff, int size);

/**
 * @brief
 *
 * @return int
 */
int main(void)
{
	bsp_initial();
	delay_1ms(60);

	char pn[64];
	char pv[64];
	int pr;

	pr = levetop_pack((unsigned char *)pn, 0xc1, 0x00, (char *)product_name, strlen(product_name));
	if (pr < 0) {
		printf("error\r\n");
	}
	_uart_send(pn, pr);

	pr = levetop_pack((unsigned char *)pv, 0xc1, 0x01, (char *)fw_version, strlen(fw_version));
	if (pr < 0) {
		printf("error\r\n");
	}
	_uart_send(pv, pr);

	printf("BL\r\n");

	ei2c_init();

	LT9211USERMAIN();
	
	while (1) {
		printf("loop...\r\n");
		delay_1ms(2000);
	}
}

void _putchar(char character)
{
	usart_data_transmit(USART1, character);
	while (RESET == usart_flag_get(USART1, USART_FLAG_TBE))
		;
}

void _uart_send(const char *buff, int size)
{
	for (int i = 0; i < size; i++) {
		usart_data_transmit(USART1, buff[i]);
		while (RESET == usart_flag_get(USART1, USART_FLAG_TBE))
			;
	}
}
