/******************************************************************************
  * @project: LT9211
  * @file: main.c
  * @author: zll
  * @company: LONTIUM COPYRIGHT and CONFIDENTIAL
  * @date: 2019.08.10
******************************************************************************/

#include "lt9211user.h"
#include "c800wlac-02-200c-b.h"
#include "lt9211.h"
#include "systick.h"


void LT9211_Reset(void)
{
	/*P11 = 0;
	Timer0_Delay1ms(10);
	P11 = 1;
	Timer0_Delay1ms(10);*/
	/*HAL_GPIO_WritePin(LT9211_RST_GPIO_Port, LT9211_RST_Pin, GPIO_PIN_RESET);
	delay_1ms(10);
	HAL_GPIO_WritePin(LT9211_RST_GPIO_Port, LT9211_RST_Pin, GPIO_PIN_SET);
	delay_1ms(10);*/
	GPIO_BOP(LT9211_RST_GPIO_PORT) = LT9211_RST_PIN;
	delay_1ms(30);
	GPIO_BC(LT9211_RST_GPIO_PORT) = LT9211_RST_PIN;
	delay_1ms(30);
	GPIO_BOP(LT9211_RST_GPIO_PORT) = LT9211_RST_PIN;
	delay_1ms(30);
}

/*********************************
//uart print function
//printdec_u32(u32 u32_dec);
//print(char* fmt, ...);
*********************************/

void LT9211USERMAIN(void)
{
	/*delay_1ms(100);
	LT9211_Reset();
	delay_1ms(3000);
	LT9211_LVDS2MIPIDSI_Config();*/
	//LCD_Reset();
	LT9211_Reset();
	delay_1ms(2500);
	LT9211_LVDS2MIPIDSI_Config();
	
}