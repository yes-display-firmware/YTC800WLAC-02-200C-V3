#ifndef __C800WLAC_02_200C_B_H__
#define __C800WLAC_02_200C_B_H__

#include "gd32e23x.h"

// Pin Define Template
/*
#define LED1_PIN                         GPIO_PIN_1
#define LED1_GPIO_PORT                   GPIOA
#define LED1_GPIO_CLK                    RCU_GPIOA
*/

// PA6 HX8394_RST
#define HX8394_RST_PIN                         GPIO_PIN_6
#define HX8394_RST_GPIO_PORT                   GPIOA
#define HX8394_RST_GPIO_CLK                    RCU_GPIOA

// PA7 LT92111_RST
#define LT9211_RST_PIN                         GPIO_PIN_7
#define LT9211_RST_GPIO_PORT                   GPIOA
#define LT9211_RST_GPIO_CLK                    RCU_GPIOA

// PB1 LT9211_INT
#define LT9211_INT_PIN                         GPIO_PIN_1
#define LT9211_INT_GPIO_PORT                   GPIOB
#define LT9211_INT_GPIO_CLK                    RCU_GPIOB

// PA9 I2C1_SCL
#define I2C0_SCL_PIN                         GPIO_PIN_9
#define I2C0_SCL_GPIO_PORT                   GPIOA
#define I2C0_SCL_GPIO_CLK                    RCU_GPIOA

// PA10 I2C1_SDA
#define I2C0_SDA_PIN                         GPIO_PIN_10
#define I2C0_SDA_GPIO_PORT                   GPIOA
#define I2C0_SDA_GPIO_CLK                    RCU_GPIOA

void GpioInitial(void);
void Uart1Initial(void);
void I2c0Initial(void);
void unitTest(void);


#endif //__C800WLAC_02_200C_B_H__