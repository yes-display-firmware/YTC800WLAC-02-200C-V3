/******************************************************************************
  * @project: LT9211
  * @file: lt9211.h
  * @author: zll
  * @company: LONTIUM COPYRIGHT and CONFIDENTIAL
  * @date: 2019.04.10
******************************************************************************/

#ifndef _LT9211_H
#define _LT9211_H
#include <stdint.h>

/******************* LVDS Input Config ********************/
#define INPUT_PORTA
//#define INPUT_PORTB

#define INPUT_PORT_NUM 1

typedef enum LVDS_FORMAT_ENUM{
    VESA_FORMAT = 0,
    JEDIA_FORMAT
} LFE;
#define LVDS_FORMAT JEDIA_FORMAT

typedef enum LVDS_MODE_ENUM{
    DE_MODE = 0,
    SYNC_MODE
} LME;
#define LVDS_MODE SYNC_MODE


typedef struct video_timing{
uint16_t hfp;
uint16_t hs;
uint16_t hbp;
uint16_t hact;
uint16_t htotal;
uint16_t vfp;
uint16_t vs;
uint16_t vbp;
uint16_t vact;
uint16_t vtotal;
uint32_t pclk_khz;
} VT;

void LT9211_LVDS2MIPIDSI_Config(void);

#endif