/**
 * @file i2c.h
 * @author Zhai Tao (zhaitao.as@outlook.com)
 * @brief 
 * @version 0.1
 * @date 2022-05-09
 * 
 * @copyright zhaitao.as@outlook.com (c) 2022
 * 
 */
#ifndef		_I2C_MASTER_H
#define		_I2C_MASTER_H

#include <stdint.h>

extern uint8_t I2CADR;
extern uint8_t HDMI_ReadI2C_Byte(uint8_t RegAddr);
extern uint8_t HDMI_ReadI2C_ByteN(uint8_t RegAddr,uint8_t *p_data,uint8_t N);
extern uint8_t HDMI_WriteI2C_Byte(uint8_t RegAddr, uint8_t d);
extern uint8_t HDMI_WriteI2C_ByteN(uint8_t RegAddr, uint8_t *d,uint8_t N);
extern void I2cMasterChoice(uint8_t i2cchannnel);


#endif
